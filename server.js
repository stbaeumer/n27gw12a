class Kunde{
    constructor(){
        this.Kennwort // änderbar durch den Kunden
        this.IdKunde
        this.Nachname 
        this.Wohnort // änderbar durch den Kunden
        this.Geburtsdatum
        this.Vorname
        this.Erwerbstaetig
    }
}

let kunde = new Kunde()
kunde.Kennwort = "123"
kunde.IdKunde = "4711"
kunde.Nachname = "Müller"
kunde.Wohnort = "Borken"

console.log(kunde.IdKunde)

class Konto{
    constructor(){
        this.Kontonummer
        this.Bankleitzahl
        this.Kontoart
        this.Iban
    }
}

const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const iban = require('iban')

const app = express()
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(cookieParser())

const mysql = require('mysql')

const dbVerbindung = mysql.createConnection({
    host: "10.40.38.110",
    user: "n27user",
    password: "BKB123456!",
    database: "dbn27"
})

dbVerbindung.connect()

dbVerbindung.query("CREATE TABLE IF NOT EXISTS konto(idKonto INT AUTO_INCREMENT, zeitstempel TIMESTAMP, idKunde INT, PRIMARY KEY(idKonto), FOREIGN KEY(idKunde) REFERENCES kunde(idKunde));", (err,rows) => {
    if(err){
        console.log(err)
    }else{
    console.log("Tabelle 'konto' erfolgreich angelegt bzw. schon vorhanden.")}

})

dbVerbindung.query("CREATE TABLE IF NOT EXISTS kontobewegung(idKonto INT, zeitstempel TIMESTAMP, text VARCHAR(150), betrag DECIMAL(15,2), gegenkonto INT, PRIMARY KEY(idKonto, zeitstempel), FOREIGN KEY(idKonto) REFERENCES kunde(idKonto));", (err,rows) => {
    if(err){
        console.log(err)
    }else{
    console.log("Tabelle 'kontobewegung' erfolgreich angelegt bzw. schon vorhanden.")}

})

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})

app.get('/',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)
        res.render('index.ejs', {                              
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})

app.get('/login',(req, res, next) => {         
    res.cookie('istAngemeldetAls', '')       
    res.render('login.ejs', {                    
    })
})

app.get('/login',(req, res, next) => {   
    res.render('login.ejs', {                    
    })
})

app.post('/',(req, res, next) => {   

    const idKunde = req.body.idKunde
    const kennwort = req.body.kennwort

    if(idKunde === kunde.IdKunde && kennwort === kunde.Kennwort){
        console.log("Der Cookie wird gesetzt")
        res.cookie('istAngemeldetAls','idKunde')
        res.render('index.ejs', {                    
        })
    }else{
        console.log("Der Cookie wird gelöscht")
        res.cookie('istAngemeldetAls','')
        res.render('login.ejs', {                    
        })
    }
})

app.get('/impressum',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)
        res.render('impressum.ejs', {                              
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})

app.get('/kontoAnlegen',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)
        res.render('kontoAnlegen.ejs', { 
            meldung : ""                                         
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})

app.post('/kontoAnlegen',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    let konto = new Konto()
    konto.Kontonummer = req.body.kontonummer
    konto.Bankleitzahl = req.body.bankleitzahl
    konto.Kontoart = req.body.kontoart        
    konto.Iban = iban.fromBBAN("DE", konto.Bankleitzahl + " " + konto.Kontonummer)

    if(idKunde){
        res.render('kontoAnlegen.ejs', {      
            meldung : "Das Konto mit der IBAN " + konto.Iban + " wurde erfolgreich angelegt!"                                    
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})

app.get('/profilBearbeiten',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)
        res.render('profilBearbeiten.ejs', { 
            meldung : ""                                         
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})

app.post('/profilBearbeiten',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
        
    kunde.Kennwort = req.body.kennwort
    kunde.Wohnort = req.body.wohnort

    if(idKunde){
        res.render('profilBearbeiten.ejs', {      
            meldung : "Das Profil wurde erfolgreich bearbeitet"
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})